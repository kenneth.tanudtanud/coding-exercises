// Get data from console
const inputs = process.argv.slice(2);

// Initialize few things
const limit = parseInt(inputs[0]);
let data = [];

// Get items from args
const getData = () => {
    for(i=1; i<=limit; i++){
        data.push(inputs[i])
    }
}

// Main
const execute = () => {
    console.log('\nLimit:', limit);
    console.log('Data:', data);

    var temp = {};
    var output = [];

    // Map count to values
    for(i=0; i<data.length; i++){
        if(data[i] in temp){
            temp[data[i]] = temp[data[i]] + 1
        } else {
            temp[data[i]] = 1
        }
    }

    console.log('Val • Count')

    /**
     * Construct Output
     * 
     * unshift - insert first item in array
     * push - insert last item in array
     */
    for (const [key, value] of Object.entries(temp)) {
        console.log(key,'  -  ', value);
        if(value == key){  
            output.unshift(key)       
        } else if(value > output[0]){
            output.unshift(key)
        } else {
            output.push(key)
        }
    }

    console.log('Output:', output, '\n')
}

// Label your steps accordingly and do something useful...
getData();
execute();